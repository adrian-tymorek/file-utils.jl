# TODO: Add recursive wildcards/regex support (that is add wildcards/regex to dir paths as well)

getmatch(m) =  m.match

"""
    ls(path::AbstractString; [join::Bool=true, sort::Bool=false]) -> Vector{String}

List dir contents in shell-like fashion. You can provide dir path or dir path with ?, * wildcards. It can only use wildcards in file name. Widlcardds in dir name not supported.

# Arguments
- `path::AbstractString`: path to look for (*, ? wildcards in file name)
- `join::Bool=true`: if true, return absolute paths, otherwise, return just the names
- `sort::Bool=false`: if true, sort file names

# Examples
```julia
julia> ls("/home/user/*.txt")
julia> ls("~/*.txt")
```
"""
function ls(path::AbstractString; join::Bool=true, sort::Bool=false)
    # Handle 2 cases: dir alone or dir + pattern
    if isdir(path)
        dirpath, pattern = path, nothing
    else
        dirpath, pattern = splitdir(path)
    end

    # Expand ~ to $HOME
    dirpath = replace(dirpath, "~" => homedir())

    # If no dir specified, use pwd
    if typeof(dirpath) <: AbstractString && length(dirpath) < 1
        dirpath = pwd()
    end

    files = readdir(dirpath, join=false, sort=sort)

    if ~isnothing(pattern)
        # Replace standard shell wildcard with RegEx
        pattern = replace(pattern, "." => "[.]")
        pattern = replace(pattern, "*" => ".*")
        pattern = replace(pattern, "?" => ".")

        # List dir and filter only files matching the pattern
        files = filter(x -> ~isnothing(x), match.(Regex(pattern), files))
        files = getmatch.(files)
    end

    # Join dir and files
    if join
        files = joinpath.(dirpath, files)
    end

    return files
end

"""
    ls() -> Vector{String}

List dir contents in shell-like fashion.

# Examples
```julia
julia> ls()
```
"""
function ls()
    ls(pwd())
end


"""
    ls(path::AbstractString, pattern::Regex; [join::Bool=true, sort::Bool=false]) -> Vector{String}

List dirs in a given location, using regex for file names.

# Arguments
- `path::AbstractString`: path to look for
- `pattern::Regex`: regex pattern for file names
- `join::Bool=true`: if true, return absolute paths, otherwise, return just the names
- `sort::Bool=false`: if true, sort file names

# Examples
```julia
julia> ls("/home/user/", r".*txt")
julia> ls("~", r".*txt")
```
"""
function ls(dirpath::AbstractString, pattern::Regex; join::Bool=true, sort::Bool=false)
    # Expand ~ to $HOME
    dirpath = replace(dirpath, "~" => homedir())

    # If no dir specified, use pwd
    if typeof(dirpath) <: AbstractString && length(dirpath) < 1
        dirpath = pwd()
    end

    files = readdir(dirpath, join=false, sort=sort)

    # List dir and filter only files matching the pattern
    files = filter(x -> ~isnothing(x), match.(pattern, files))
    files = getmatch.(files)

    # Join dir and files
    if join
        files = joinpath.(dirpath, files)
    end

    return files
end

"""
    ls(pattern::Regex; [join::Bool=true, sort::Bool=false]) -> Vector{String}

List dirs in a current directory, using regex for file names.

# Arguments
- `pattern::Regex`: regex pattern for file names
- `join::Bool=true`: if true, return absolute paths, otherwise, return just the names
- `sort::Bool=false`: if true, sort file names

# Examples
```julia
julia> ls("/home/user/", r".*txt")
julia> ls("~", r".*txt")
```
"""
function ls(pattern::Regex, join::Bool=true, sort::Bool=false)
    ls(pwd(), pattern, join=join, sort=sort)
end


"""
    tree(inpath::AbstractString; mode::AbstractString="dirs+files") -> Vector{String}

List all dirs in a current directory and and all subdirectories.

# Arguments
- `inpath::AbstractString`: path to look for files
- `mode::AbstractString="dirs+files`: "dirs" - return only subdirs, "files" - return only files, "dirs+files" - return all

# Examples
```julia
julia> ls("/home/user/")
julia> ls("~")
```
"""
function tree(inpath::AbstractString; mode::AbstractString="dirs+files")
    inpath = replace(inpath, "~" => homedir())

    outpaths = []
    for i in walkdir(inpath)
        root, dirs, files = i

        if occursin("dirs", mode)
            push!(outpaths, root)
        end

        if occursin("files", mode)
            if typeof(files) == Vector{String} && length(files) > 0
                outpaths = vcat(outpaths, joinpath.(root, files))
            end
        end
    end

    return outpaths
end


"""
    findfile(inpath::AbstractString; regex::Union{Regex, Nothing}=nothing, name::Union{AbstractString, Nothing}=nothing, mode::AbstractString="files") -> Vector{String}

List all dirs in a current directory and and all subdirectories, filtered by regex or shell-like patterns.

# Arguments
- `inpath::AbstractString`: path to look for files
- `name::AbstractString`: name pattern (*, ? wildcards in file name). Default is nothing
- `regex::Regex`: name regex pattern. Default is nothing
- `mode::AbstractString="dirs+files`: "dirs" - return only subdirs, "files" - return only files, "dirs+files" - return all. Default is "dirs+files"

# Examples
```julia
julia> findfile("/home/user/", regex=r".+txt")
julia> findfile("~", regex=r".+jl")
```
"""
function findfile(inpath::AbstractString; regex::Union{Regex, Nothing}=nothing, name::Union{AbstractString, Nothing}=nothing, mode::AbstractString="files")
    inpath = replace(inpath, "~" => homedir())
    outpaths = tree(inpath, mode=mode)

    if ~isnothing(name) && isnothing(regex)
        regex = replace(name, "." => "[.]")
        regex = replace(regex, "*" => ".*")
        regex = replace(regex, "?" => ".")
        regex = Regex(regex)
    end

    if ~isnothing(regex)
        outpaths = filter(x -> ~isnothing(x), match.(regex, outpaths))
        outpaths = getmatch.(outpaths)
    end

    return outpaths
end
